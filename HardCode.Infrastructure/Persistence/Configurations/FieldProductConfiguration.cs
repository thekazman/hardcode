﻿using HardCode.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Infrastructure.Persistence.Configurations
{
    internal class FieldProductConfiguration : IEntityTypeConfiguration<FieldProduct>
    {
        public virtual void Configure(EntityTypeBuilder<FieldProduct> builder)
        {
            builder.HasKey(i => new { i.FieldId, i.ProductId });
            builder.Property(p => p.Value).IsRequired().HasMaxLength(500);
            builder.HasOne(fp => fp.Field)
                .WithMany(f => f.FieldProducts)
                .HasForeignKey(fp => fp.FieldId)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(fp => fp.Product)
                .WithMany(p => p.FieldProducts)
                .HasForeignKey(fp => fp.ProductId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
