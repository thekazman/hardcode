﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using HardCode.Domain.Entities;

namespace HardCode.Infrastructure.Persistence.Configurations
{
    internal class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public virtual void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasKey(e => e.CategoryId);
            builder.HasIndex(e => e.CategoryId).IsUnique();
            builder.Property(p => p.CreatedAt).IsRequired().HasDefaultValueSql("now()");
            builder.Property(p => p.DeletedAt).IsRequired(false);
            builder.Property(p => p.CategoryName).IsRequired().HasMaxLength(128);
        }
    }
}
