﻿using HardCode.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Infrastructure.Persistence.Configurations
{
    internal class FieldConfiguration : IEntityTypeConfiguration<Field>
    {
        public virtual void Configure(EntityTypeBuilder<Field> builder)
        {
            builder.HasKey(e => e.FieldId);
            builder.HasIndex(e => e.FieldId).IsUnique();
            builder.Property(p => p.CreatedAt).IsRequired().HasDefaultValueSql("now()");
            builder.Property(p => p.FieldName).IsRequired().HasMaxLength(128);
            builder.HasOne(f => f.Category)
                .WithMany(i => i.Fields)
                .HasForeignKey(f => f.CategoryId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
