﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using HardCode.Domain.Entities;

namespace HardCode.Infrastructure.Persistence.Configurations
{
    internal class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public virtual void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(e => e.ProductId);
            builder.HasIndex(e => e.ProductId).IsUnique();
            builder.Property(p => p.CreatedAt).IsRequired().HasDefaultValueSql("now()");
            builder.Property(p => p.ProductName).IsRequired().HasMaxLength(100);
            builder.Property(p => p.ProductDescription).IsRequired().HasMaxLength(500);
            builder.Property(p => p.ProductPrice).HasColumnType("decimal(18, 2)");
            builder.Property(p => p.ProductImage).IsRequired().HasMaxLength(255);
            builder.HasOne(p => p.Category)
                .WithMany(c => c.Products)
                .HasForeignKey(p => p.CategoryId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
