﻿using HardCode.Domain.Entities;
using HardCode.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Infrastructure.Persistence
{
    public class AppDbContext : DbContext
    {
        public DbSet<Category> Category { get; set; }
        public DbSet<Field> Field { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<FieldProduct> FieldProduct { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new FieldConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new FieldProductConfiguration());
        }
    }
}
