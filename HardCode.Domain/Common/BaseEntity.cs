﻿namespace HardCode.Domain.Common
{
    public class BaseEntity
    {
        public DateTimeOffset CreatedAt { get; set; }
    }
}
