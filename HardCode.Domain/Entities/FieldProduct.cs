﻿namespace HardCode.Domain.Entities
{
    public class FieldProduct
    {
        public int FieldId { get; set; }
        public Field Field { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public string Value { get; set; }
    }
}
