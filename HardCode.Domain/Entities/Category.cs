﻿using HardCode.Domain.Common;

namespace HardCode.Domain.Entities
{
    public class Category : BaseEntity
    {
        public int CategoryId { get; set; }
        public DateTimeOffset? DeletedAt { get; set; }
        public string CategoryName { get; set; }
        public ICollection<Field> Fields { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
