﻿using HardCode.Domain.Common;

namespace HardCode.Domain.Entities
{
    public class Field : BaseEntity
    {
        public int FieldId { get; set; }
        public string FieldName { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<FieldProduct> FieldProducts { get; set; }
    }
}
