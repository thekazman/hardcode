﻿using HardCode.Domain.Common;

namespace HardCode.Domain.Entities
{
    public class Product : BaseEntity
    {
        public int ProductId { get; set; }
        public string ProductImage { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public double? ProductPrice { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<FieldProduct> FieldProducts { get; set; }
    }
}
