using HardCode.Application.Common.Settings;
using HardCode.Infrastructure;

var builder = WebApplication.CreateBuilder(args);
builder.Services.Configure<Appsettings>(builder.Configuration.GetSection(nameof(Appsettings)));
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddInfrastructureServices(builder.Configuration);
var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseAuthorization();
app.MapControllers();
app.Run();
