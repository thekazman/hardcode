﻿namespace HardCode.Application.Common.Settings
{
    public class Appsettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
        public string BaseUri { get; set; }
        public string Environment { get; set; }

    }

    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
    }
}
